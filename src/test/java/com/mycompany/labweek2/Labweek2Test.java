/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.labweek2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class Labweek2Test {
    
    public Labweek2Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     
     */
    
    @Test
    public void testswitchTurn_X_O(){
        char turn = 'X';
        char result = Labweek2.switchTurn(turn);
        
        assertEquals('O',result);
    
    }
    @Test
    public void testcheckDraw(){
    int nturn = 9;
    boolean result = Labweek2.checkDraw(nturn);
        assertTrue(result);
    
    
    }
    @Test
    public void testcheckRow_Xwin_row1(){
    char[][] grid = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
    int row = 0;
    char turn = 'X';
    boolean result = Labweek2.checkRow(grid,row,turn);
        assertTrue(result);
    }
    @Test
    public void testcheckRow_Xwin_row2(){
    char[][] grid = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
    int row = 1;
    char turn = 'X';
    boolean result = Labweek2.checkRow(grid,row,turn);
        assertTrue(result);
    }
    @Test
    public void testcheckRow_Owin_row3(){
    char[][] grid = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
    int row = 2;
    char turn = 'O';
    boolean result = Labweek2.checkRow(grid,row,turn);
        assertTrue(result);
    }
    @Test
    public void testcheckCol_Xwin_col1(){
    char[][] grid = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
    int col = 0;
    char turn = 'X';
    boolean result = Labweek2.checkCol(grid,col,turn);
    assertTrue(result); 
    }
    @Test
    public void testcheckCol_Xwin_col2(){
    char[][] grid = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
    int col = 1;
    char turn = 'X';
    boolean result = Labweek2.checkCol(grid,col,turn);
    assertTrue(result); 
    }
    @Test
    public void testcheckCol_Owin_col3(){
    char[][] grid = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
    int col = 2;
    char turn = 'O';
    boolean result = Labweek2.checkCol(grid,col,turn);
    assertTrue(result); 
    }
    @Test
    public void testcheckDia_Owin_upperRightTOlowerLeft(){
    char[][] grid = {{'-','-','O'},{'-','O','-'},{'O','-','-'}};
    int row = 0;
    int col = 2;
    char turn ='O'; 
    boolean result = Labweek2.checkDia(grid,row,col,turn);
    assertTrue(result); 
    }
    @Test
    public void testcheckDia_Owin_upperLeftTOlowerRight(){
    char[][] grid = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
    int row = 0;
    int col = 0;
    char turn ='O'; 
    boolean result = Labweek2.checkDia(grid,row,col,turn);
    assertTrue(result); 
    }
    
    

}
