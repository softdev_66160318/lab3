/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.labweek2;

import java.util.Scanner;

/**
 *
 * @author PN24TH
 */
public class Labweek2 {
    static char turn = 'X';
    static int nturn;
    static int row;
    static int col;
    
    private static char[][] grid = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    
    public static void main(String[] args) {
        printWelcome();
      
   do{
       
       showGrid();
       showTurn();
       inputRowCol();
       if (isEnded()){
           showResult();
           break;}
       
       switchTurn(turn);
       
       
       
      
       
       
   }while(!isEnded());
   
  }
  private static void printWelcome(){
      System.out.println("Welcome to XO game");
      
  
  }
  private static void  showGrid(){
      for (int r = 0;r<3;r++){
          for (int c = 0;c<3;c++){
              System.out.print(grid[r][c] + " ");
             
            }
          System.out.println("");
          
      }}
      
   private static void inputRowCol(){
       Scanner scanner = new Scanner(System.in);
       System.out.println("Please input row and col ");
       row =scanner.nextInt()-1;
       col =scanner.nextInt()-1;
       if (grid[row][col] != '-'){
             System.out.println("Occupied");
             turn = (turn == 'X')?'O':'X';
      }else{
       grid[row][col] = turn;
       nturn++;
  }}
   
  static void showTurn(){
            System.out.println("Turn "+turn);
      }
  static char switchTurn(char turn1){
  turn = (turn1 == 'X')?'O':'X';
  return turn;
  }
  static boolean isEnded(){
      if (checkDraw(nturn)||checkRow(grid,row,turn)||checkCol(grid,col,turn)||checkDia(grid,row,col, turn)){
      return true;}
      else{
      return false;}
  
  }
      
  static boolean checkRow(char[][] grid,int row,char turn){
  if (grid[row][0]==turn&&grid[row][1]==turn&&grid[row][2]==turn){return true;}
  else{return false;}
  
  }
  static boolean checkCol(char[][] grid,int col,char turn){
  if (grid[0][col] == turn&& grid[1][col] == turn && grid[2][col] == turn){
      
      return true;
  }
  else{
      
      
     
        return false;
  
  }}
  static boolean checkDia(char[][] grid,int row,int col,char turn){
  if (row == col && grid[0][0] == turn && grid[1][1] == turn && grid[2][2] == turn) {
            return true;
        }
      if (row + col == 2 && grid[0][2] == turn && grid[1][1] == turn && grid[2][0] == turn) {
            return true;
  
  }else{return false;}}
  
static boolean checkDraw(int nturn){
   if (nturn == 9){
          
          return true;
   }else{return false;}
  
  }
  
  
  static void showResult(){
      
      
      if(nturn ==9){System.out.println("------DRAWWW----------");
      }else{
      System.out.println("Player "+turn +" Wins");}
        
        
  
  }}








